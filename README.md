# AcitivtyStreams support for Hugo

This is a [Hugo module](https://gohugo.io/categories/hugo-modules), you can use in [Hugo](https://gohugo.io/), to generate [ActivityStreams](https://www.w3.org/ns/activitystreams) representations of posts.

## How to use

You can use this module by adding it to your Hugo config (example of a YAML Hugo configuration file):

```yaml
module:
  imports:
   # other imports ...
   # The hugo-activitystreams module:
   - path: git.jlel.se/jlelse/hugo-activitystreams
```

You also need to specify the [custom output format](https://gohugo.io/templates/output-formats/) `activity` for the kinds `index` and `page`. When you add a custom output format, you need to make sure to also include the [default ones](https://gohugo.io/templates/output-formats/#default-output-formats).

```yaml
outputs:
  home:
    - "html" # Default
    - "rss" # Default
    - "activity"
  page:
    - "html" # Default
    - "activity"
```

And finally (if you want to use use your website as an [ActivityPub](https://www.w3.org/TR/activitypub/) actor - middleware to do that with Hugo is coming soon...), you need to configure two params: `inbox` with the ActivityPub inbox URL and `publickeypem` with the PEM format of the public key of the key you use to sign activities.

```yaml
params:
  # Your other params...
  # ActivityPub params
  activitypub:
    inbox: https://blog.example/activitypub/de/inbox
    publickeypem: -----BEGIN PUBLIC KEY-----...
```

### Note

By default the [ActivityPub](https://www.w3.org/TR/activitypub/) actor has the IRI of the language-based homepage. Example:

- Domain: jlelse.blog (a Hugo blog with two configured languages, default language "en")
  - English actor:
    - Username: @en@jlelse.blog
    - IRI: https://jlelse.blog/
  - German actor:
    - Username: @de@jlelse.blog
    - IRI: https://jlelse.blog/de/

## License

This project is MIT licensed. See the license [here](./LICENSE).